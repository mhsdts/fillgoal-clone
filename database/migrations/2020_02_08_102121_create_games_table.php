<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug', 200);
            $table->unsignedBigInteger('home_id');
            $table->unsignedBigInteger('away_id');
            $table->unsignedBigInteger('league_id');
            $table->unsignedInteger('match_number')->default(1);
            $table->date('starting_date');
            $table->time('starting_time');
            $table->integer('home_score')->nullable();
            $table->integer('away_score')->nullable();

            $table->foreign('home_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('away_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
