<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('name_fr');
            $table->string('surname')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('position'); // Defender or Midfielder or Forward
            $table->unsignedBigInteger('team_id')->nullable();
            $table->unsignedBigInteger('national_team_id')->nullable();
            $table->foreign('team_id')
                ->references('id')->on('teams')->onDelete('set null');
            $table->foreign('national_team_id')
                ->references('id')->on('national_teams')->onDelete('set null');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
