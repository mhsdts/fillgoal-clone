<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $systemAdmin = new User();
        $systemAdmin->name = 'System Administrator';
        $systemAdmin->email = 'admin@sports.com';
        $systemAdmin->password = bcrypt('123456');
        $systemAdmin->email_verified_at = now();
        $systemAdmin->save();
    }
}
