<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeagueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name_ . app()->getLocale(),
            'slug' => $this->slug,
            'upcoming' => $this->whenLoaded('games',
                UpcomingGameResource::collection($this->games()->where('home_score', null)->get())),
        ];
    }
}
