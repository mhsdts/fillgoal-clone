<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UpcomingGameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'home_team' => TeamResource::make($this->home),
            'away_team' => TeamResource::make($this->away),
            'starting_date' => date('d M', strtotime($this->starting_date)),
            'starting_time' => date('H:i', strtotime($this->starting_time)),
            'match_number' => $this->match_number,
            'league' => $this->whenLoaded('league', LeagueResource::make($this->league)),
        ];
    }
}
