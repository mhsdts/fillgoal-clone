<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $img_directory = env('NEWS_IMAGE_DIRECTORY');
        $lang = '';

        return [
            'id' => $this->id,
            'title_en' => $this->title_en,
            'title_ar' => $this->title_ar,
            'title_fr' => $this->title_fr,
            'description_en' => $this->description_en,
            'description_ar' => $this->description_ar,
            'description_fr' => $this->description_fr,
            'body_en' => $this->body_en,
            'body_ar' => $this->body_ar,
            'body_fr' => $this->body_fr,
            'banner_filename' => $this->banner_filename,
            'created_at' => $this->created_at,
            'banner_url' => Storage::disk('public')->url($img_directory . $this->banner_filename),
        ];
    }
}
