<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = getLang();
        $video_directory = env('MAIN_VIDEOS_DIRECTORY');
        $video_url = Storage::disk('public')->url($video_directory . $this->filename);

        if (is_null($lang)) {
            return [
                'id' => $this->id,
                'video_url' => $video_url,
                'title_en' => $this->title_en,
                'title_ar' => $this->title_ar,
                'title_fr' => $this->title_fr,
                'filename' => $this->filename,
                'created_at' => $this->created_at
            ];

        } else {
            $title = "";
            switch ($lang) {
                case 'en':
                    $title = $this->title_en;
                    break;
                case 'fr':
                    $title = $this->title_fr;
                    break;
                case 'ar':
                    $title = $this->title_ar;
                    break;
                default:
                    $title = $this->title_en;
            }
            return  [
                'id' => $this->id,
                'video_url' => $video_url,
                'title' => $title,
            ];
        }
    }
}
