<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name_en' => $this->name_en,
            'name_ar' => $this->name_ar,
            'name_fr' => $this->name_fr,
            'surname' => $this->surname,
            'birth_date' =>  $this->birthdate,
            'position' => $this->position,
            'team_id' => $this->team_id,
            'national_team_id' => $this->national_team_id,
        ];
    }
}
