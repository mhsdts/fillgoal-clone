<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $img_directory = env('TEAM_IMAGE_DIRECTORY');
        return [
            'name' => $this->name_ . app()->getLocale(),
            'slug' => $this->slug,
            'flag' => $this->flag ? Storage::disk('public')->url($img_directory . $this->flag) : null,
            'players' => PlayerResource::collection($this->whenLoaded('players')),
            'games_home' => GameResource::collection('gamesHome'),
            'games_away' => GameResource::collection('gamesAway')
        ];
    }
}
