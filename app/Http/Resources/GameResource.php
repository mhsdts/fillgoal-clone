<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'home_id' => $this->home_id,
            'away_id' => $this->away_id,
            'league_id' => $this->league_id,
            'match_number' => $this->match_number,
            'starting_date' => $this->starting_date,
            'starting_time' => $this->starting_time,
            'home_score' => $this->home_score,
            'away_score' => $this->away_score,
            'home_team' => new TeamResource($this->whenLoaded('homeTeam')),
            'away_team' => new TeamResource($this->whenLoaded('awayTeam')),
            'league' => new TeamResource($this->whenLoaded('league')),
            'players' => PlayerResource::collection($this->whenLoaded('players'))
        ];
    }
}
