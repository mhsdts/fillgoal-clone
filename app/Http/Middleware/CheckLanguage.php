<?php

namespace App\Http\Middleware;

use Closure;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('Accept-Language')
        && in_array($request->header('Accept-Language'), config('app.locales'))
            ? $request->header('Accept-Language') : config('app.locale');
        app()->setLocale($locale);
        return $next($request);
    }
}
