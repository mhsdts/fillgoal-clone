<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        $data = Team::with(['players', 'gamesAway', 'gamesHome'])->orderBy('created_at', 'desc')->get();
        $data = TeamResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Team::find($id);
        $data = new TeamResource($data);

        return response()->json(['data' => $data], 200);
    }
}
