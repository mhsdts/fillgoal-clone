<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\VideoResource;
use App\Models\Videos;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        $per_page = request()->header('per_page') ? request()->header('per_page') : 5;
        $videos = Videos::orderBy('created_at', 'DESC')->paginate($per_page);
        $videos = VideoResource::collection($videos)->response()->getData(true);


        return response()->json($videos, 200);
    }

    public function getById($id)
    {
        $video = Videos::find($id);
        $video = new VideoResource($video);

        return response()->json($video, 200);
    }
}
