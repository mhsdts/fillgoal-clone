<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PlayerResource;
use App\Models\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function index()
    {
        $data = Player::orderBy('created_at', 'desc')->get();
        $data = PlayerResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Player::find($id);
        $data = new PlayerResource($data);

        return response()->json(['data' => $data], 200);
    }
}
