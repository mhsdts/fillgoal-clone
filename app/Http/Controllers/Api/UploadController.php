<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function uploadImage(ImageRequest $imageRequest) {

        $modelType = $imageRequest->query('model');
        $image_file = $imageRequest->file('image_file');
        $temp_directory  = env('TEMP_MAGE_DIRECTORY');
        $filename = rand(1, 1000) . time() . '.' . $image_file->getClientOriginalExtension();

        switch ($modelType) {
            case 'post':
                Storage::disk('local')->put($temp_directory . $filename, file_get_contents($image_file));
                break;
            default:
                return \response()->json(['message' => 'Please provide correct model name'], 400);
        }
        return response()->json(['banner_filename' => $filename], 200);
    }
}
