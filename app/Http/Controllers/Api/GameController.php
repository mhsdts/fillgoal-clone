<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\GameResource;
use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index()
    {
        $data = Game::orderBy('created_at', 'desc')->get();
        $data = GameResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Game::with(['league', 'player','homeTeam','awayTeam'])->find($id);
        $data = new GameResource($data);

        return response()->json(['data' => $data], 200);
    }
}
