<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LeagueResource;
use App\Models\League;
use Illuminate\Http\Request;

class LeaguesController extends Controller
{
    public function index()
    {
        $data = League::with('games')->get();
        $data = LeagueResource::collection($data);
        return response()->json([
            'data' => $data
        ], 200);
    }

    public function getById($id)
    {
        $data = Legaue::find($id);
        $data = new LeagueResource($data);

        return response()->json(['data' => $data], 200);
    }
}
