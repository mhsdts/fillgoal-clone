<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //
    public function index(Request $request)
    {
        $per_page = request()->header('per_page') ? request()->header('per_page') : 5;

        $data = News::orderBy('created_at', 'desc')->paginate($per_page);
        $data = NewsResource::collection($data)->response()->getData(true);

        return response()->json( $data, 200);
    }

    public function getById($id)
    {
        $data = News::find($id);
        if (is_null($data)) return response()->json(['message'=> 'This news is not found'],400);

        $data = new NewsResource($data);
        return response()->json( $data, 200);
    }

    public function getByTeamId($teamId) {
        $data = News::where('team_id', $teamId)->get();
        if (is_null($data)) return response()->json(['message' => 'This news is not found'], 400);

        $data = NewsResource::collection($data);
        return response()->json(['data' => $data], 200);
    }

    public function getByLeagueId($leagueId) {
        $data = News::where('league_id', $leagueId)->get();
        if (is_null($data)) return response()->json(['message' => 'This news is not found'], 400);

        $data = NewsResource::collection($data);
        return response()->json(['data' => $data], 200);
    }
}
