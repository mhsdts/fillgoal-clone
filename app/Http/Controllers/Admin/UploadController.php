<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\VideoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function uploadImage(ImageRequest $request)
    {

//        $modelType = $imageRequest->query('model');
        $image_file = $request->file('image_file');
        $temp_directory = env('TEMP_IMAGE_DIRECTORY');
        $filename = rand(1, 1000) . time() . '.' . $image_file->getClientOriginalExtension();

        Storage::disk('local')->put($temp_directory . $filename, file_get_contents($image_file));

        return response()->json(['img_filename' => $filename], 200);
    }

    public function uploadVideo(VideoRequest $request)
    {
//        $modelType = $imageRequest->query('model');
        $video_file = $request->file('video_file');
        $temp_directory = env('TEMP_VIDEOS_DIRECTORY');
        $filename = rand(1, 1000) . time() . '.' . $video_file->getClientOriginalExtension();

        Storage::disk('local')->put($temp_directory . $filename, file_get_contents($video_file));

        return response()->json(['video_filename' => $filename], 200);
    }
}
