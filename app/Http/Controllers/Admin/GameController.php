<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GameRequest;
use App\Http\Resources\GameResource;
use App\Models\Game;
use Illuminate\Support\Str;

class GameController extends Controller
{
    public function index()
    {
        $data = Game::with(['awayTeam', 'homeTeam', 'players'])->orderBy('created_at', 'desc')->get();
        $data = GameResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Game::with(['awayTeam', 'homeTeam', 'players'])->find($id);
        $data = new GameResource($data);

        return response()->json(['data' => $data], 200);
    }

    public function store(GameRequest $request)
    {
        $attributes = $request->all();
        $attributes['slug'] = Str::slug(Str::random(6), '-');
        $data = Game::create($attributes);

        return response()->json(['data' => new GameResource($data), 'message' => 'Game Created successfully'], 200);
    }

    public function update(GameRequest $request, $id)
    {
        $attributes = $request->all();

        $gameInDb = Game::find($id);
        if (is_null($gameInDb)) return response()->json(['message' => 'This game dont exist'], 400);

        $gameInDb->update($attributes);

        return response()->json(['message' => 'Game data Updated Successfully'], 200);
    }

    public function delete($id)
    {
        $game = Game::find($id);
        if (!is_null($game)) $game->delete();

        else return response()->json(['message' => 'This game doesnt exist'], 400);

        return response()->json(['message' => 'Game deleted successfully'], 200);
    }
}
