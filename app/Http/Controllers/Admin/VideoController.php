<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VideoFilesRequest;
use App\Http\Resources\VideoResource;
use App\Models\Videos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    protected $temp_directory, $video_directory;

    public function __construct()
    {
        $this->middleware('auth:api');

        $this->temp_directory = env('TEMP_VIDEOS_DIRECTORY');
        $this->video_directory = env('MAIN_VIDEOS_DIRECTORY');
    }

    public function index()
    {
        $videos = Videos::orderBy('created_at', 'DESC')->get();
        $videos = VideoResource::collection($videos);

        return response()->json( $videos, 200);
    }

    public function getById($id)
    {
        $video = Videos::find($id);
        $video = new VideoResource($video);

        return response()->json($video, 200);
    }

    public function store(VideoFilesRequest $request)
    {
        $attributes = $request->all();
        if (isset($attributes['filename']) && $attributes['filename'] !== '') {
            Storage::disk('local')->move($this->temp_directory . $attributes['filename'],
                $this->video_directory . $attributes['filename']);
        }

        $video = Videos::create($attributes);
        $video = new VideoResource($video);

        return response()->json(['data' => $video], 200);
    }

    public function update(Request $request, $id)
    {
        $video = Videos::find($id);
        $attributes = $request->all();
        if (is_null($video)) return response()->json(['message' => 'This video not found'], 400);

        if (!is_null($attributes['filename']) && ($video->filename != $attributes['filename'])) {
            Storage::disk('local')->delete($this->video_directory . $video->filename);

            if (isset($attributes['filename']) && $attributes['filename'] !== '') {
                Storage::disk('local')->move($this->temp_directory . $attributes['filename'],
                    $this->video_directory . $attributes['filename']);
            }
        }

        $video->update($attributes);

        return response()->json(['message' => 'Video updated successfully'], 200);
    }

    public function delete($id)
    {
        $video = Videos::find($id);
        if (is_null($video)) return response()->json(['message' => 'This video not found'], 400);
        Storage::disk('local')->delete($this->video_directory . $video->filename);

        $video->delete();

        return response()->json(['message' => 'Video deleted successfully'], 200);
    }
}
