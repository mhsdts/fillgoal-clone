<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    protected $temp_Directory, $img_directory;

    public function __construct()
    {
        $this->middleware('auth:api');

        $this->temp_Directory = env('TEMP_IMAGE_DIRECTORY');
        $this->img_directory = env('NEWS_IMAGE_DIRECTORY');
    }

    public function index()
    {
        $data = News::orderBy('created_at', 'desc')->get();
        $data = NewsResource::collection($data);

        return response()->json($data, 200);
    }

    public function getById($id)
    {
        $data = News::find($id);
        $data = new NewsResource($data);

        return response()->json($data, 200);
    }

    public function store(NewsRequest $request)
    {
        $attributes = $request->all();
        if (isset($attributes['banner_filename']) && $attributes['banner_filename'] !== '') {
            Storage::disk('local')->move($this->temp_Directory . $attributes['banner_filename'],
                $this->img_directory . $attributes['banner_filename']);
        }

        $data = News::create($attributes);

        return response()->json(['data' => $data, 'message' => 'News Created successfully'], 200);
    }

    public function update(NewsRequest $request, $id)
    {
        $attributes = $request->all();
        $newInDb = News::find($id);

        if (is_null($newInDb)) return response()->json(['message' => 'this news doesnt exist'], 400);

        if (!is_null($attributes['banner_filename']) && ($newInDb->banner_filename != $attributes['banner_filename'])) {
            Storage::disk('local')->delete($this->img_directory . $newInDb->banner_filename);

            if (isset($attributes['banner_filename']) && $attributes['banner_filename'] !== '') {
                Storage::disk('local')->move($this->temp_Directory . $attributes['banner_filename'],
                    $this->img_directory . $attributes['banner_filename']);
            }
        }
        $data = $newInDb->update($attributes);
        return response()->json(['message' => 'News Updated Successfully'], 200);
    }

    public function delete($id)
    {
        $news = News::find($id);

        Storage::disk('local')->delete($this->img_directory . $news->banner_filename);
        if (!is_null($news)) $news->delete();
        else return response()->json(['message' => 'This news doesnt exist'], 400);

        return response()->json(['message' => 'news deleted successfully'], 200);
    }
}
