<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeamRequest;
use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    protected $temp_directory, $img_directory;

    public function __construct()
    {
        $this->temp_directory = env('TEMP_IMAGE_DIRECTORY');
        $this->img_directory = env('TEAM_IMAGE_DIRECTORY');
    }

    public function index()
    {
        $data = Team::orderBy('created_at', 'desc')->get();
        $data = TeamResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Team::find($id);
        $data = new TeamResource($data);

        return response()->json(['data' => $data], 200);
    }

    public function store(TeamRequest $request)
    {
        $attributes = $request->all();
        $attributes['slug'] = Str::slug($request->input('name_en'), '-');
        if (isset($attributes['flag']) && $attributes['flag'] !== '') {
            Storage::disk('local')->move($this->temp_directory . $attributes['flag'],
                $this->img_directory . $attributes['flag']);
        }

        $data = Team::create($attributes);
        $data->leagues()->sync($request->input('league_id'));

        return response()->json(['data' => $data, 'message' => 'Team Created successfully'], 200);
    }

    public function update(TeamRequest $request, $id)
    {
        $attributes = $request->all();

        $teamInDb = Team::find($id);
        if (is_null($teamInDb)) return response()->json(['message' => 'this team dont exist'], 400);

        if (!is_null($attributes['flag']) && ($teamInDb->flag != $attributes['flag'])) {
            Storage::delete($this->img_directory . $teamInDb->flag);

            if (isset($attributes['flag']) && $attributes['flag'] !== '') {
                Storage::disk('local')->move($this->temp_directory . $attributes['flag'],
                    $this->img_directory . $attributes['flag']);
            }
        }
        $teamInDb->update($attributes);
        $teamInDb->leagues()->sync($request->input('league_id'));

        return response()->json(['message' => 'Team Updated Successfully'], 200);
    }

    public function delete($id)
    {
        $team = Team::find($id);
        if (!is_null($team)) {
            if (!is_null($team->flag)) {
                Storage::disk('local')->delete($this->img_directory . $team->flag);
            }
            $team->delete();
        } else return response()->json(['message' => 'This team doesnt exist'], 400);

        return response()->json(['message' => 'Team deleted successfully'], 200);
    }
}
