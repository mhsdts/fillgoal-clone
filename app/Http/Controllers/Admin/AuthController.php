<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public $successStatus = 200;
    public $unauthorizedStatus = 401;
    public function login(Request $request){

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json($success, $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], $this->unauthorizedStatus);
        }
    }
}
