<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlayerRequest;
use App\Http\Resources\PlayerResource;
use App\Models\Player;

class PlayerController extends Controller
{

    public function index()
    {
        $data = Player::orderBy('created_at', 'desc')->get();
        $data = PlayerResource::collection($data);

        return response()->json(['data' => $data], 200);
    }

    public function getById($id)
    {
        $data = Player::find($id);
        $data = new PlayerResource($data);

        return response()->json(['data' => $data], 200);
    }

    public function store(PlayerRequest $request)
    {

        $attributes = $request->all();
        $data = Player::create($attributes);

        return response()->json(['data' => $data, 'message' => 'Player Created successfully'], 200);
    }

    public function update(PlayerRequest $request, $id)
    {
        $attributes = $request->all();

        $playerInDb = Player::find($id);
        if (is_null($playerInDb)) return response()->json(['message' => 'this Player dont exist'], 400);

        $playerInDb->update($attributes);

        return response()->json(['message' => 'Player data Updated Successfully'], 200);
    }

    public function delete($id)
    {
        $player = Player::find($id);
        if (!is_null($player)) $player->delete();

        else return response()->json(['message' => 'This player doesnt exist'], 400);

        return response()->json(['message' => 'Player deleted successfully'], 200);
    }
}
