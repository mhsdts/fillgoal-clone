<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeagueRequest;
use App\Http\Resources\LeagueResource;
use App\Models\League;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LeaguesController extends Controller
{
    public function index()
    {
        $data = League::with('games')->get();
        $data = LeagueResource::collection($data);
        return response()->json([
            'data' => $data
        ], 200);
    }

    public function getById($id)
    {
        $data = League::find($id);
        $data = new LeagueResource($data);

        return response()->json(['data' => $data], 200);
    }

    public function store(LeagueRequest $request) {
        $league = League::create([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'name_fr' => $request->input('name_fr'),
            'slug' => Str::slug($request->input('name-en'),'-')
        ]);
        $data = new LeagueResource($league);
        return response()->json(['data' => $data], 200);
    }

    public function update(LeagueRequest $request, $id) {
        $league = League::find($id);
        if (is_null($league)) return response()->json(['message'=> 'this league doesnt exist'], 400);

        $league->update([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'name_fr' => $request->input('name_fr'),
            'slug' => Str::slug($request->input('name-en'),'-')
        ]);

        return response()->json(['message' => 'League updated successfully!'], 200);
    }

    public function delete($id) {
        $league = League::find($id);
        if (!is_null($league)) $league->delete();
        else return response()->json(['message' => 'This league doesnt exist'], 400);

        return response()->json(['message' => 'League deleted successfully'], 200);
    }

}
