<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoFilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filename' => 'required|string|min:2|max:255',
            'title_en' => 'required|string|min:2|max:255',
            'title_ar' => 'required|string|min:2|max:255',
            'title_fr' => 'required|string|min:2|max:255'
        ];
    }
}
