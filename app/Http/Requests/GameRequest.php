<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'home_id' => 'required|number',
        'away_id' => 'required|number',
        'league_id' => 'required|number',
        'match_number' => 'nullable',
        'starting_date' => 'required|date',
        'starting_time' => 'required|date_format:H:i',
        'home_score' => 'nullable|number',
        'away_score' => 'nullable|number',
        ];
    }
}
