<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function players()
    {
        return $this->hasManyThrough(Player::class, GamePlayer::class);
    }
    public function awayTeam()
    {
        return $this->belongsTo(Team::class, 'away');
    }
    public function homeTeam()
    {
        return $this->belongsTo(Team::class, 'home');
    }
    public function league()
    {
        return $this->belongsTo(League::class, 'league_id');
    }
}
