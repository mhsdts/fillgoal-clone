<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';

    protected $fillable = ['filename', 'title_en', 'title_ar', 'title_fr'];
}
