<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    public function players()
    {
        return $this->hasMany(Player::class, 'team_id');
    }

    public function gamesHome()
    {
        return $this->hasMany(Game::class, 'home');
    }

    public function gamesAway()
    {
        return $this->hasMany(Game::class, 'away');
    }

    public function gamePlayers()
    {
        return $this->hasManyThrough(Player::class, GamePlayer::class);
    }

    public function leagues()
    {
        return $this->hasManyThrough(League::class, LeagueTeam::class);
    }
}
