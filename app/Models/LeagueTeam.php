<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeagueTeam extends Model
{
    protected $table = 'league_team';

    public function teams()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    public function league()
    {
        return $this->belongsTo(League::class, 'league_id');
    }
}
