<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GamePlayer extends Model
{
    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id');
    }
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id');
    }
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
