<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title_en',
        'title_ar',
        'title_fr',
        'description_en',
        'description_ar',
        'description_fr',
        'body_en',
        'body_ar',
        'body_fr',
        'banner_filename'
    ];

    public function team() {
        return $this->belongsTo(Team::class,'team_id');
    }

    public function league() {
        return $this->belongsTo(League::class, 'league_id');
    }
}
