<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class League extends Model
{
    use SoftDeletes;

    public function teams()
    {
        return $this->hasManyThrough(Team::class, LeagueTeam::class);
    }

    public function games()
    {
        return $this->hasMany(League::class, 'league_id');
    }
}
