<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function games()
    {
        return $this->hasMany(GamePlayer::class);
    }

}
