<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NationalTeam extends Model
{
    protected $table = 'national_teams';
}
