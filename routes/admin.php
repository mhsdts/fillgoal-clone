<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register ADMIN API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "API" middleware group. Enjoy building your API!
|
*/

//login route
Route::post('login', 'AuthController@login')->middleware('throttle:10,1');

//News routes
Route::get('news', 'NewsController@index');
Route::post('news', 'NewsController@store');

Route::get('news/{id}', 'NewsController@getById');
Route::put('news/{id}', 'NewsController@update');

Route::delete('news/{id}', 'NewsController@delete');

Route::get('news/team/{id}', 'NewsController@getByTeamId');
Route::get('news/league/{id}', 'NewsController@getByLeagueId');

//videos route
Route::get('videos', 'VideoController@index');
Route::post('videos', 'VideoController@store');

Route::get('videos/{id}', 'VideoController@getById');
Route::put('videos/{id}', 'VideoController@update');

Route::delete('videos/{id}', 'VideoController@delete');

//upload routes
Route::post('upload/image', 'UploadController@uploadImage');
Route::post('upload/video', 'UploadController@uploadVideo');
