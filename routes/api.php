<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//News routes
Route::get('news', 'NewsController@index');
Route::get('news/{id}', 'NewsController@getById');

Route::get('news/team/{id}', 'NewsController@getByTeamId');
Route::get('news/league/{id}', 'NewsController@getByLeagueId');

//videos route
Route::get('videos', 'VideoController@index');
Route::get('videos/{id}', 'VideoController@getById');
